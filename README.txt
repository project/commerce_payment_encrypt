Commerce Payment Transaction Encrypt (commerce_payment_encrypt)
---------------------------------------------------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Note about security
 * Note about this module development

INTRODUCTION
------------

This module protects payment transactions, encrypting them into the
database. This module doesn't alter user experience. In case of database
hacking, this sensitive data would be useless for the hacker.

REQUIREMENTS
------------

This module requires the following modules:
 * It uses the encrypt module, providing the encryption method.

INSTALLATION
------------

 * PAY ATTENTION : Proceed first to a full backup of your database,
     especially the commerce_payment_transaction table : it is going to be
     updated.
   If it goes wrong, those datas could be lost forever !

 * Navigate to "Administer › Site building > modules", Install encrypt
     module, available on http://drupal.org/project/encrypt

 * Install as you would normally install a contributed Drupal module. Visit:
     https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
     for further information.

 * Go on the commerce_payment_encrypt settings page
     (admin/config/system/encrypt/commerce_payment_encrypt) as
     super-administrator (user1) or as a user with appropriate permissions
     ("manage commerce payment encryption") : read the
     help about 'Key Provider' and store it outside the database.

 * Keep a copy of the encryption key in a secure location.

CONFIGURATION
-------------

 * Easy to use :
   - Install the Commerce Payment Transaction Encrypt (commerce_payment_encrypt)
       module.
   - Go to admin/config/system/encrypt/commerce_payment_encrypt .
   - Choose encrypt option from select and encrypt all payment transactions.
   - Choose to add this to CRON.
   - Uninstall it or disable it : it will disable the payment transactions
       encryption for all payment transactions.

 * To access the administration options page, navigate to "configuration >
     System > Encrypt > Commerce Payment Encrypt".
   Only users with "manage commerce payment encrypt" permission are
     able to see it.

 * For security reason, it is highly recommended :
   - To store encryption key into a file outside the webroot, instead of into
       the database (read "installation" section and "Note" section).
   - KEEP A COPY OF THE ENCRYPTION KEY into a secure location : the
       encryption key is not saved on database backup, AND LOSING THIS KEY
       WOULD DEFINITELY PREVENT ACCESS TO ALL PAYMENT TRANSACTIONS!!


MAINTAINERS
-----------
 * Alexandru (lexsoft) - https://www.drupal.org/user/3487887
 * When you procced to regular database backup of your website, be careful
     to save both database AND the file that contains the encryption key.

NOTE ABOUT SECURITY:
--------------------
 * Obfuscation versus Security (extract from
     http://drupal.org/project/encrypt)

 * It should be noted the difference between obfuscation [1] and security.
   It is important to understand how security is at work and where the
     points of failure are.

 * By default, this module uses a key that is stored in your database,
     while the main use of this module is to store encrypted data in the
     database. This is actually just an example of obfuscation because if the
     database itself is compromised, all the appropriate parts are available
     to retrieve that data, though much more complicated once the data has
     been encrypted.

 * When you put your key outside the webroot, the encrypted text and key
     are now in two distinct parts of the system which will have a lot less
     likelihood of being compromised at the same time. It is still important
     to know that this module does not make your data completely secure, it
     just allows a level of security that Drupal does not inherently provide,
     and in fact there are many levels that need to be thought about to have
     fully secure data.

 * [1] : https://en.wikipedia.org/wiki/Obfuscation


NOTE ABOUT THIS MODULE DEVELOPMENT:
-----------------------------------

BECAUSE THIS PROGRAM HANDLES SENSITIVE DATA, I RECOMMAND TO PERFORM
DATABASE BACKUP BEFORE USING IT.
This module has been developed respecting Drupal security guide
("Writing secure code") and drupal best practices.

The program is provided "as is" without warranty of any kind, either
expressed or implied, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose. The
entire risk as to the quality and performance of the program is with
you. Should the program prove defective, you assume the cost of all
necessary servicing, repair or correction.
