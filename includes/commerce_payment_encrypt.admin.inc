<?php

/**
 * @file
 * Functions for encrypting/decrypting commerce payment transactions.
 */

/**
 * Form builder; Commerce Payment Encrypt options.
 *
 * @see commerce_payment_encrypt_admin_form_validate()
 * @see commerce_payment_encrypt_admin_form_submit()
 */
function commerce_payment_encrypt_admin_form($form, &$form_state) {
  $form['option_type'] = [
    '#type' => 'select',
    '#title' => t('Commerce Payment Encrypt Options'),
    '#description' => t('<p>Select to encrypt or decrypt.</p>
                         <p>This will run a DB Batch selecting <em>payload</em> column from 
                         {{commerce_payment_transaction}} table</p>
                         <p>Please note this could affect site performances for few minutes 
                         depending on how many payment transactions exists in the payload column.</p>'),
    '#description_display' => 'before',
    '#options' => [
      'none' => '-- Select --',
      'encrypt' => 'Encrypt',
      'decrypt' => 'Decrypt',
      'change' => 'Re-encrypt',
    ],
  ];

  $form['commerce_transaction_cron_default_time'] = array(
    '#type' => 'select',
    '#title' => t('Commerce Payment Encrypt CRON'),
    '#description' => t('<p>Select the site wide default to automatically encrypt payment transactions. </p>
                         <p>This will run the DB Batch and check & encrypt unencrypted payment transactions.</p>'),
    '#options' => _commerce_payment_encrypt_get_cron_time_options(),
    '#default_value' => variable_get('commerce_payment_encrypt_cron_default_time', COMMERCE_PAYMENT_ENCRYPT_DO_NOT_ENCRYPT),
  );

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];
  return $form;
}

/**
 * Validate commerce_payment_encrypt module options.
 */
function commerce_payment_encrypt_admin_form_validate($form, &$form_state) {
  if (!user_access('manage commerce payment encryption')) {
    form_set_error('', t('You are not allowed to change options.'));
  }
}

/**
 * Implements hook_FORM_ID_submit().
 *
 * Encrypts or decrypts payment transactions.
 */
function commerce_payment_encrypt_admin_form_submit($form, &$form_state) {
  module_load_include('inc', 'commerce_payment_encrypt', 'includes/commerce_payment_encrypt.encrypt');

  if (!empty($form_state['values']['option_type'])) {
    commerce_payment_encrypt_update_encrypt($form_state['values']['option_type']);
  }
  variable_set('commerce_payment_encrypt_cron_default_time', $form_state['values']['commerce_transaction_cron_default_time']);

}

/**
 * Form builder; administer commerce_payment_encrypt module options.
 *
 * @see commerce_payment_encrypt_admin_form_validate()
 * @see commerce_payment_encrypt_admin_form_submit()
 * @ingroup forms
 */
function commerce_payment_encrypt_admin_config_form(&$form, &$form_state) {

  form_load_include($form_state, 'inc', 'commerce_payment_encrypt', 'includes/commerce_payment_encrypt.admin');

  $form['commerce_payment_encrypt'] = array(
    '#type' => 'fieldset',
    '#title' => t('Commerce Payment Encrypt Settings'),
    '#description' => t('Read this if you encounter some <strong>performance issues</strong>.'),
    '#group' => 'settings',
    '#weight' => -1,
  );

  $perf_case = '<p>' . t('You may meet performance issues, due to the %module module, <strong>on one specific use case</strong> matching both conditions below :', array('%module' => t('Commerce Payment Encrypt'))) . '</p>';

  $perf_case .= '<p>' . t('Thoses performance issues <strong>may happen on</strong> :') . '</p>';

  $conditions = array();
  $conditions[] = t('Encrypting all payload transactions first time on admin/config/system/encrypt/commerce_payment_encrypt page');
  $conditions[] = t('If encryption is triggered on CRON run on weekly or monthly basis');

  $perf_case .= theme('item_list', array('items' => $conditions));
  $perf_case .= '<p>' . t('For more details please read <strong>README.txt</strong> in %module module', array('%module' => t('Commerce Payment Encrypt'))) . '</p>';
  $form['commerce_payment_encrypt']['settings'] = array(
    '#markup' => $perf_case,
  );

  // Remove delete button.
  if (isset($form['actions']['delete'])) {
    $form['actions']['delete']['#access'] = FALSE;
  }

  if (!module_exists('dbee')) {
    // Re-encrypt all commerce payment payload transactions.
    $form['#validate'][] = 'commerce_payment_encrypt_admin_config_form_validate';
    $form['actions']['submit']['#submit'][] = 'commerce_payment_encrypt_admin_config_form_submit';
    $form['commerce_payment_encrypt']['commerce_reencrypt'] = array(
      '#type' => 'checkbox',
      '#title' => '<strong>' . t('Re-encrypted all commerce payment payload transactions') . '</strong>',
      '#description' => '<p>' . t('If checked, all commerce payment payload transactions will be re-encrypted. This could take a while depending on how many transactions exists.') . '</p><p>' . t('NOTE: You first need to encrypt commerce payment payload transactions on the admin/config/system/encrypt/commerce_payment_encrypt page.') . '</p>',
      '#default_value' => 0,
      '#weight' => 45,
    );
  }
  else {
    // Avoid conflict with dbee module, add a button instead to decrypt all
    // data.
    $form['commerce_payment_encrypt']['note'] = array(
      '#markup' => '<p>' . t('Decrypt Commerce Payment Transaction Payload Data') . ' <strong>' . t('before making any changes to encryption options.') . '</strong></p>',
    );
    $form['commerce_payment_encrypt']['commerce_reencrypt'] = array(
      '#name' => 'commerce_reencrypt',
      '#type' => 'submit',
      '#value' => t('Decrypt Payload Data'),
      '#prefix' => '<div class="button-wrapper">',
      '#attributes' => array('alt' => t('Decrypt Payload Data')),
      '#suffix' => '</div>',
      '#submit' => array('commerce_payment_encrypt_decrypt_all'),
      '#weight' => 45,
    );

  }

  // Disable the edition of the name and description.
  $form['label']['#disabled'] = TRUE;
  $form['description']['#disabled'] = TRUE;
  $form['description']['#type'] = 'textfield';
  $form['name']['#disabled'] = TRUE;

  // Can not be disabled.
  if (!empty($form['general_settings']['enabled']['#default_value'])) {
    $form['general_settings']['enabled']['#disabled'] = TRUE;
  }

  // Always provide the default method.
  if (empty($form['method_settings']['encrypt_encryption_method']['#options']['default'])) {
    $methods = encrypt_get_encryption_methods();
    $form['method_settings']['encrypt_encryption_method']['#options']['default'] = $methods['default']['title'] . ' <span class="encrypt-warning">' . t('(Deprecated)') . '</span>';
  }
}

/**
 * Validate commerce_payment_encrypt module options.
 */
function commerce_payment_encrypt_admin_config_form_validate($form, &$form_state) {
  if (!user_access('manage commerce payment encryption')) {
    form_set_error('', t('You are not allowed to change options.'));
  }
  if (!form_get_errors()) {
    $config_name = $form_state['values']['name'];
    $prev_config = encrypt_get_config($config_name);
    $prev_method = (!empty($prev_config['method'])) ? $prev_config['method'] : FALSE;
    $new_method = (!empty($form_state['values']['encrypt_encryption_method'])) ? (string) $form_state['values']['encrypt_encryption_method'] : FALSE;
    if ($new_method && $new_method != $prev_method) {
      // Method has changed.
      foreach (array('default', 'none') as $fixed_method) {
        if ($new_method == $fixed_method || $prev_method == $fixed_method) {
          // Because the commerce_payment_encrypt module act differently with
          // this method.
          if (empty($form_state['values']['commerce_reencrypt'])) {
            $form_state['values']['commerce_reencrypt'] = TRUE;
          }
        }
      }
    }
  }
}

/**
 * Submit commerce_payment_encrypt module options.
 */
function commerce_payment_encrypt_admin_config_form_submit($form, &$form_state) {

  // Re-encrypt all commerce payment payload transactions.
  if (!empty($form_state['values']['commerce_reencrypt'])) {
    module_load_include('inc', 'commerce_payment_encrypt', 'includes/commerce_payment_encrypt.encrypt');
    commerce_payment_encrypt_update_encrypt('change');
  }
}

/**
 * Decrypt all commerce payment transactions.
 */
function commerce_payment_encrypt_decrypt_all() {
  module_load_include('inc', 'commerce_payment_encrypt', 'includes/commerce_payment_encrypt.encrypt');
  commerce_payment_encrypt_update_encrypt('decrypt');
}

/**
 * Display a table will all encryption configurations.
 *
 * Alter the data to prevent deleting the commerce_payment_encrypt
 *   configuration.
 */
function commerce_payment_encrypt_configs_list() {
  module_load_include('inc', 'encrypt', 'includes/encrypt.admin');
  $build = encrypt_configs_list();
  if (!empty($build['encrypt_configs_list_table']['#rows'])) {
    $search = array(
      url(ENCRYPT_MENU_PATH . '/delete/commerce_payment_encrypt'),
      url(ENCRYPT_MENU_PATH . '/default/commerce_payment_encrypt'),
    );
    foreach ($build['encrypt_configs_list_table']['#rows'] as $row_id => $cols) {
      foreach ($cols as $col_id => $cell) {
        foreach ($search as $url) {
          if (!empty($cell['data']) && strpos($cell['data'], $url) !== FALSE) {
            // Link for deleting or make default for commerce_payment_encrypt
            // config : delete the cell.
            $build['encrypt_configs_list_table']['#rows'][$row_id][$col_id]['data'] = '';
          }
        }
      }
    }
  }
  return $build;
}
