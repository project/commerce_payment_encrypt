<?php

/**
 * @file
 * Functions for encrypting/decrypting commerce payment transactions.
 */

/**
 * Encrypts or decrypts commerce payment transactions.
 *
 * This function encrypts or decrypts all commerce payment transactions from
 * the commerce_payment_transaction table. It is used when the
 * commerce_payment_encrypt module is installed, enabled or disabled, it acts
 * as simple callback or batch operation if there are more than 15 payment
 * transactions in database.
 *
 * @param string $action
 *   A string. Possible values are 'decrypt', 'encrypt', and 'change'.
 *
 * @return bool
 *   A boolean False if $action is not correctly set or NULL.
 */
function commerce_payment_encrypt_update_encrypt($action) {
  if (!in_array($action, array('decrypt', 'encrypt', 'change'))) {
    return FALSE;
  }
  $do_message = (($action == 'encrypt') ? t('encrypted') : (($action == 'decrypt') ? t('decrypted') : t('re-encrypted')));

  $batch = array(
    'operations' => array(
      array(
        'commerce_payment_encrypt_all_batch_proceed',
        array($action),
      ),
    ),
    'title' => t('@crypt all commerce payment transactions', array('@crypt' => $do_message)),
    'init_message' => t('Encrypting Batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Encrypting Batch has encountered an error.'),
    'file' => drupal_get_path('module', 'commerce_payment_encrypt') . '/includes/commerce_payment_encrypt.encrypt.inc',
    'finished' => 'commerce_payment_encrypt_all_batch_finished',
  );
  batch_set($batch);
  // From Drush, explicitly launch the batch proceess.
  if (drupal_is_cli() && function_exists('drush_main')) {
    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    // Process the batch.
    drush_backend_batch_process();
  }
}

/**
 * Batch process for changing encryption of payment transaction.
 *
 * (15 payment transactions per operation).
 *
 * @param object $context
 *   Parameter $context is an array that will contain information about the
 *   status of the batch. The values in $context will retain their
 *   values as the batch progresses.
 */
function commerce_payment_encrypt_all_batch_proceed($action, &$context) {

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_transaction_id'] = 0;
    $context['sandbox']['max'] = db_select('commerce_payment_transaction')->fields(NULL, array('transaction_id'))->countQuery()->execute()->fetchField();
    $context['results']['commerce_payment_encrypt_action'] = $action;
  }

  // 15 nodes at a time without a timeout.
  $limit = 15;

  // With each pass through the callback, retrieve the next group of payment
  // transactions.
  $result = db_select('commerce_payment_transaction', 'p')
    ->fields('p', array(
      'transaction_id',
      'order_id',
      'payment_method',
      'payload',
    ))
    ->condition('transaction_id', $context['sandbox']['current_transaction_id'], '>')
    ->orderBy('transaction_id')
    ->range(0, $limit)
    ->execute();

  foreach ($result as $row) {

    // Here we actually perform our processing on the current order.
    $order_id = $row->order_id;
    $transaction_id = $row->transaction_id;
    _commerce_payment_encrypt_insert_data($row, $action);

    // Store some result for post-processing in the finished callback.
    $context['results'][] = check_plain($order_id);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_transaction_id'] = $transaction_id;
    $context['message'] = t('processing order : %name (id : @id)', array(
      '@id' => $order_id,
      '%name' => $row->payment_method,
    ));
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
  else {
    $context['finished'] = 1;
  }
}

/**
 * Batch process finished callback for changing encryption.
 *
 * @see commerce_payment_encrypt_all_batch_proceed()
 */
function commerce_payment_encrypt_all_batch_finished($success, $results, $operations) {

  // Count the totul number of payment transactions.
  $n_payment_transactions = (isset($results)) ? count($results) : 0;
  $action = (isset($results['commerce_payment_encrypt_action'])) ? $results['commerce_payment_encrypt_action'] : FALSE;

  if ($action) {
    $do_message = (($action == 'encrypt') ? t('encrypted') : (($action == 'decrypt') ? t('decrypted') : t('re-encrypted')));
    $message_arg = array(
      '%crypted' => $do_message,
      '@total_payment_transactions' => $n_payment_transactions,
    );
    if ($success) {
      $message_str = 'All commerce payment transactions have been %crypted (concerning @total_payment_transactions commerce payment transactions)';
      $message_status = TRUE;
    }
    else {
      $message_str = 'Failing on the operation : not all payment transactions have been %crypted.';
      $message_str .= ' If you are received this error when decrypting it is probably because the commerce payment transactions are already decrypted';
      $message_status = FALSE;
    }
    // Watchdog changes.
    $wd_status = ($message_status) ? WATCHDOG_INFO : WATCHDOG_ERROR;
    watchdog('commerce_payment_encrypt', $message_str, $message_arg, $wd_status);
    $mess_status = ($message_status) ? 'status' : 'error';

    drupal_set_message(t($message_str, $message_arg), $mess_status);

    if ($n_payment_transactions == 0) {
      throw new DrupalUpdateException('The commerce payment transactions could not be decrypted !');
    }
  }
}

/**
 * Update payload column in commerce_payment_transaction table.
 *
 * @param object $row
 *   An array. Including order_id, transaction_id and payload encrypted &
 *   decrypted data.
 * @param string $action
 *   A string. Possible values are 'decrypt', 'encrypt', and 'change'.
 */
function _commerce_payment_encrypt_insert_data($row, $action = 'encrypt') {

  $order_id = $row->order_id;
  $transaction_id = $row->transaction_id;
  $payload = $row->payload;

  // Query DB for encrypted payment transactions.
  $query_check_if_order_is_encrypted = db_select('commerce_payment_encrypt', 'p')
    ->fields('p', array(
      'status',
    ))
    ->condition('order_id', $order_id)
    ->execute();
  $check_encryption_status = $query_check_if_order_is_encrypted->fetchAll();

  // Check if encryption exists.
  $method = _commerce_payment_encrypt_decrypt_method($payload);

  // $action defined in commerce_payment_encrypt_config_form_submit().
  switch ($action) {
    case 'encrypt':
      // Encrypt every order.
      if (!$method) {
        $data_updated = encrypt($payload);

        if ($query_check_if_order_is_encrypted->rowCount() == 0) {
          _commerce_payment_encrypt_status('insert', $order_id, 1);
        }
        else {
          _commerce_payment_encrypt_status('update', $order_id, 1);
        }
        if ($check_encryption_status[0]->status == 0 || $query_check_if_order_is_encrypted->rowCount() == 0) {
          _commerce_payment_encrypt_payload($data_updated, $transaction_id, $order_id);
        }
      }

      break;

    case 'decrypt':
      // Decrypt every order.
      if (!empty($method)) {
        $data_updated = decrypt($payload);

        if ($query_check_if_order_is_encrypted->rowCount() == 0) {
          _commerce_payment_encrypt_status('insert', $order_id, 0);
        }
        else {
          _commerce_payment_encrypt_status('update', $order_id, 0);
        }
        if ($check_encryption_status[0]->status == 1 || $query_check_if_order_is_encrypted->rowCount() == 0) {
          _commerce_payment_encrypt_payload($data_updated, $transaction_id, $order_id);
        }
      }
      break;

    case 'change':
      if (!empty($method)) {
        $data_updated = decrypt($payload);
        $check_method = _commerce_payment_encrypt_decrypt_method($data_updated);
        if (!$check_method) {
          $data_updated = encrypt($data_updated);
        }
        if ($check_encryption_status[0]->status == 1 || $query_check_if_order_is_encrypted->rowCount() == 0) {
          _commerce_payment_encrypt_payload($data_updated, $transaction_id, $order_id);
        }
      }
      break;
  }

}

/**
 * Return the encryption method, or false if the data is not encrypted.
 *
 * Encryption method provides by the encrypt module are 'mcrypt_aes_cbc',
 * 'default', 'none'.
 *
 * @param string $string
 *   The string to test, retrieved from the database.
 *
 * @return string
 *   A string the encryption method, or FALSE if the data is ot encrypted.
 */
function _commerce_payment_encrypt_decrypt_method($string) {
  $to_decrypt = utf8_decode($string);
  $unserialized = @unserialize($to_decrypt);
  // If the data is serialized, assume it as been build by the encrypt module.
  return (is_array($unserialized) && !empty($unserialized['method'])) ? $unserialized['method'] : FALSE;
}

/**
 * Log encrypted & decrypted payment transactions in database.
 *
 * @param string $log
 *   A string. Possible values are 'insert', 'update'.
 * @param int $order_id
 *   A integer. Entity reference order ID.
 * @param bool $status
 *   A boolean. Possible values are '1' order is encrypted & '0' order is
 *   decrypted.
 *
 * @return bool
 *   A boolean False if $log is not correctly set or NULL.
 */
function _commerce_payment_encrypt_status($log, $order_id, $status) {

  if (!in_array($log, array('insert', 'update'))) {
    return FALSE;
  }

  switch ($log) {
    case 'insert':
      // If the order does not exists log a new record.
      db_insert('commerce_payment_encrypt')
        ->fields([
          'order_id' => $order_id,
          'status' => $status,
        ])
        ->execute();
      break;

    case 'update':
      db_update('commerce_payment_encrypt')
        ->fields(array(
          'status' => $status,
        ))
        ->condition('order_id', $order_id)
        ->execute();
      break;

    default:
      // This action is not allowed.
      return FALSE;
  }
}

/**
 * Update payload column in commerce_payment_transaction table.
 *
 * @param string $data_updated
 *   An array. Encrypted & Decrypted data.
 * @param int $transaction_id
 *   A integer. The transaction to be updated.
 * @param int $order_id
 *   A integer. Entity reference order ID.
 */
function _commerce_payment_encrypt_payload($data_updated, $transaction_id, $order_id) {

  // Update the order payment_transaction payload.
  db_update('commerce_payment_transaction')
    ->fields(array('payload' => $data_updated))
    ->condition('transaction_id', $transaction_id)
    ->condition('order_id', $order_id)
    ->execute();

}
